package com.hnttechs.www.dan;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by dell on 8/20/16.
 */
public class DBHandler extends SQLiteOpenHelper implements NewsListener  {
    private static final int DB_VERSION = 7;
    private static final String DB_NAME = "NewsDatabase.db";
    private static final String TABLE_NEWS = "news_table";
    private static final String TABLE_IMPORTANT = "importantnews_table";
    private static final String TABLE_READ = "read_table";
    private static final String KEY_ID = "_id";
    private static final String KEY_POSTID = "_postid";
    private static final String KEY_TITLE = "_title";
    private static final String KEY_CONTENT = "_content";
    private static final String KEY_POSTDATE = "_postdate";
    private static final String KEY_URL = "_url";
    private static final String KEY_POST_URL = "_posturl";

    String CREATE_NEWS_TABLE = "CREATE TABLE "+TABLE_NEWS+" ("+KEY_ID+" INTEGER PRIMARY KEY,"+KEY_POSTID
            +" TEXT unique,"+KEY_TITLE+" TEXT,"+KEY_CONTENT+" TEXT,"+KEY_POSTDATE+" TEXT,"+KEY_URL
            +" TEXT,"+KEY_POST_URL+" TEXT)";
    String DROP_NEWS_TABLE = "DROP TABLE IF EXISTS "+TABLE_NEWS;


    String CREATE_IMPORTANTNEWS_TABLE = "CREATE TABLE "+TABLE_IMPORTANT+" ("+KEY_ID+" INTEGER PRIMARY KEY,"+KEY_POSTID
            +" TEXT unique,"+KEY_TITLE+" TEXT,"+KEY_CONTENT+" TEXT,"+KEY_POSTDATE+" TEXT,"+KEY_URL
            +" TEXT,"+KEY_POST_URL+" TEXT)";
    String DROP_IMPORTANTNEWS_TABLE = "DROP TABLE IF EXISTS "+TABLE_IMPORTANT;


    String CREATE_READ_TABLE = "CREATE TABLE "+TABLE_READ+" ("+KEY_ID+" INTEGER PRIMARY KEY,"+KEY_POSTID
            +" TEXT unique)";
    String DROP_READ_TABLE = "DROP TABLE IF EXISTS "+TABLE_READ;


    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_NEWS_TABLE);
        db.execSQL(CREATE_IMPORTANTNEWS_TABLE);
        db.execSQL(CREATE_READ_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_NEWS_TABLE);
        db.execSQL(DROP_IMPORTANTNEWS_TABLE);
        db.execSQL(DROP_READ_TABLE);
        onCreate(db);
    }

    @Override
    public void addNews(News news) {
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(KEY_POSTID, news.getPost_id());
            values.put(KEY_TITLE, news.getTitle());
            values.put(KEY_CONTENT,news.getContent());
            values.put(KEY_POSTDATE, news.getPost_date());
            values.put(KEY_URL, news.getUrl());
            values.put(KEY_POST_URL, news.getPost_url());
            db.insert(TABLE_NEWS, null, values);
            db.close();
        }catch (Exception e){
            Log.e("problem", e + "");
        }
    }

    public void addImportantNews(ImportantNews importantnews) {
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(KEY_POSTID, importantnews.getPost_id());
            values.put(KEY_TITLE, importantnews.getTitle());
            values.put(KEY_CONTENT,importantnews.getContent());
            values.put(KEY_POSTDATE, importantnews.getPost_date());
            values.put(KEY_URL, importantnews.getUrl());
            values.put(KEY_POST_URL, importantnews.getPost_url());
            db.insertWithOnConflict(TABLE_IMPORTANT, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            db.close();
        }catch (Exception e){
            Log.e("problem", e + "");
        }
    }


    public void addRead(Read read) {
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(KEY_POSTID, read.getPost_id());
            db.insert(TABLE_READ, null, values);
            db.close();
        }catch (Exception e){
            Log.e("problem", e + "");
        }
    }


    @Override
    public ArrayList<News> getAllNews() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<News> newsList = null;
        try{
            newsList = new ArrayList<News>();
            String QUERY = "SELECT * FROM "+TABLE_NEWS;
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    News news = new News();
                    news.setId(cursor.getInt(0));
                    news.setPost_id(cursor.getString(1));
                    news.setTitle(cursor.getString(2));
                    news.setContent(cursor.getString(3));
                    news.setPost_date(cursor.getString(4));
                    news.setUrl(cursor.getString(5));
                    news.setPost_url(cursor.getString(6));
                    newsList.add(news);
                }
            }
            db.close();
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return newsList;
    }

    public ArrayList<ImportantNews> getAllImportant() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ImportantNews> importantnewsList = null;
        try{
            importantnewsList = new ArrayList<ImportantNews>();
            String QUERY = "SELECT * FROM "+TABLE_IMPORTANT;
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    ImportantNews importantnews = new ImportantNews();
                    importantnews.setId(cursor.getInt(0));
                    importantnews.setPost_id(cursor.getString(1));
                    importantnews.setTitle(cursor.getString(2));
                    importantnews.setContent(cursor.getString(3));
                    importantnews.setPost_date(cursor.getString(4));
                    importantnews.setUrl(cursor.getString(5));
                    importantnews.setPost_url(cursor.getString(6));
                    importantnewsList.add(importantnews);
                }
            }
            db.close();
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return importantnewsList;
    }

    @Override
    public int getNewsCount() {
        int num = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM "+TABLE_NEWS;
            Cursor cursor = db.rawQuery(QUERY, null);
            num = cursor.getCount();
            db.close();
            return num;


        }catch (Exception e){
            Log.e("error",e+"");
        }
        return 0;
    }


    public int getImportantNewsCount() {
        int num = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM "+TABLE_IMPORTANT;
            Cursor cursor = db.rawQuery(QUERY, null);
            num = cursor.getCount();
            db.close();
            return num;
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return 0;
    }


    public void deleteNewsData() {
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            db.execSQL("delete from "+ TABLE_NEWS);
            db.close();
        }catch (Exception e){
            Log.e("problem", e + "");
        }
    }


    public void deleteImportantNewsData() {
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            db.execSQL("delete from " + TABLE_IMPORTANT);
            db.close();
        }catch (Exception e){
            Log.e("problem", e + "");
        }
    }


    public boolean CheckIsDataAlreadyInDBorNot(String post_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String QUERY = "SELECT * FROM " + TABLE_IMPORTANT + " where " + KEY_POSTID + " = " + post_id;
        Cursor cursor = db.rawQuery(QUERY, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }


    public boolean IsRead(String post_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String QUERY = "SELECT * FROM " + TABLE_READ + " where " + KEY_POSTID + " = " + post_id;
        Cursor cursor = db.rawQuery(QUERY, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

}
