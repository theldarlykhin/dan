package com.hnttechs.www.dan;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dell on 8/26/16.
 */
public class Dos_Donts extends AppCompatActivity {

    private Toolbar toolbar;
    static int[] water_flood_array = {
            R.drawable.before_1,
            R.drawable.before_2,
            R.drawable.before_3,
            R.drawable.before_4,
            R.drawable.before_5,
            R.drawable.before_6,
            R.drawable.before_7,
            R.drawable.before_8,
            R.drawable.before_9,
            R.drawable.before_10,
            R.drawable.during_1,
            R.drawable.during_2,
            R.drawable.during_3,
            R.drawable.during_4,
            R.drawable.after_1,
            R.drawable.after_2,
            R.drawable.after_3
    };


    static int[] storm = {
            R.drawable.s_before0,
            R.drawable.s_before1,
            R.drawable.s_before2,
            R.drawable.s_before3,
            R.drawable.s_during0,
            R.drawable.s_during1,
            R.drawable.s_after0,
            R.drawable.s_after1,
            R.drawable.s_after2,
            R.drawable.s_after3,
            R.drawable.s_after4,
            R.drawable.s_after5
    };

    static int[] fire = {
            R.drawable.fire77,
            R.drawable.fire78,
            R.drawable.fire79,
            R.drawable.fire80,
            R.drawable.fire81,
            R.drawable.fire82,
            R.drawable.fire83,
            R.drawable.fire84,
            R.drawable.fire85,
            R.drawable.fire86,
            R.drawable.fire87,
            R.drawable.fire71,
            R.drawable.fire72,
            R.drawable.fire73,
            R.drawable.fire74,
            R.drawable.fire75,
            R.drawable.fire76,
            R.drawable.fire88,
            R.drawable.fire89,
            R.drawable.fire90,
            R.drawable.fire91,
            R.drawable.fire92,
            R.drawable.fire93

    };


    static int[] thunder = {
            R.drawable.thunder1,
            R.drawable.thunder2,
            R.drawable.thunder3,
            R.drawable.thunder4,
            R.drawable.thunder5,
            R.drawable.thunder6,
            R.drawable.thunder7,
            R.drawable.thunder8,
            R.drawable.thunder9,
            R.drawable.thunder10,
            R.drawable.thunder11,
            R.drawable.thunder12,
            R.drawable.thunder13
    };



    static int[] draught = {
            R.drawable.draught_crop1,
            R.drawable.draught_crop2,
            R.drawable.draught_crop3,
            R.drawable.draught_crop4,
            R.drawable.draught_crop5,
            R.drawable.draught_crop6,
            R.drawable.draught_crop7,
            R.drawable.draught_crop8,
            R.drawable.draught_crop9,
            R.drawable.draught_crop10,
            R.drawable.draught_crop11,
            R.drawable.draught_crop12,
            R.drawable.draught_crop13,
            R.drawable.draught_crop14,
            R.drawable.draught_crop15,
            R.drawable.draught_crop16
    };



    static int[] tsunami = {
            R.drawable.tsunami1,
            R.drawable.tsunami2,
            R.drawable.tsunami3,
            R.drawable.tsunami4,
            R.drawable.tsunami5,
            R.drawable.tsunami6,
            R.drawable.tsunami7,
            R.drawable.tsunami8,
            R.drawable.tsunami9,
            R.drawable.tsunami10,
            R.drawable.tsunami11,
            R.drawable.tsunami12,
            R.drawable.tsunami13,
            R.drawable.tsunami14,
            R.drawable.tsunami15,
            R.drawable.tsunami16,
            R.drawable.tsunami17
    };



    static int[] earthquake = {
            R.drawable.	earthquake51,
            R.drawable.	earthquake52,
            R.drawable.	earthquake53,
            R.drawable.	earthquake54,
            R.drawable.	earthquake55,
            R.drawable.	earthquake56,
            R.drawable.	earthquake57,
            R.drawable.	earthquake58,
            R.drawable.	earthquake59,
            R.drawable.	earthquake60,
            R.drawable.	earthquake61,
            R.drawable.	earthquake62,
            R.drawable.	earthquake63,
            R.drawable.	earthquake64,
            R.drawable.	earthquake65,
            R.drawable.	earthquake66,
            R.drawable.	earthquake67,
            R.drawable.	earthquake68
    };



    static int[] landslide = {
            R.drawable.landslide1,
            R.drawable.landslide2,
            R.drawable.landslide3,
            R.drawable.landslide4,
            R.drawable.landslide5,
            R.drawable.landslide6,
            R.drawable.landslide7,
            R.drawable.landslide8
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dos_donts);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.do_dont_actionbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.textColorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        TextView txt_flood = (TextView)findViewById(R.id.txt_flood);
        TextView txt_storm = (TextView)findViewById(R.id.txt_storm);
        TextView txt_fire = (TextView)findViewById(R.id.txt_fire);
        TextView txt_thunder = (TextView)findViewById(R.id.txt_thunder);
        TextView txt_draught = (TextView)findViewById(R.id.txt_draught);
        TextView txt_tsunami = (TextView)findViewById(R.id.txt_tsunami);
        TextView txt_earthquake = (TextView)findViewById(R.id.txt_earthquake);
        TextView txt_landslide = (TextView)findViewById(R.id.txt_landslide);
        Typeface tf_zawgyi = Typeface.createFromAsset(getAssets(), "fonts/zawgyione.ttf");
        txt_flood.setTypeface(tf_zawgyi);
        txt_storm.setTypeface(tf_zawgyi);
        txt_fire.setTypeface(tf_zawgyi);
        txt_thunder.setTypeface(tf_zawgyi);
        txt_draught.setTypeface(tf_zawgyi);
        txt_tsunami.setTypeface(tf_zawgyi);
        txt_earthquake.setTypeface(tf_zawgyi);
        txt_landslide.setTypeface(tf_zawgyi);
        ImageView water_flood = (ImageView)findViewById(R.id.img_water_flood);
        water_flood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent before_water_flood = new Intent(getBaseContext(), water_flood_Fragment.class);
                startActivity(before_water_flood);


            }
        });


        ImageView storm = (ImageView)findViewById(R.id.img_storm);
        storm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent storm = new Intent(getBaseContext(), storm_Fragment.class);
                startActivity(storm);
            }
        });


        ImageView fire = (ImageView)findViewById(R.id.img_fire);
        fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fire = new Intent(getBaseContext(),fire_Fragment.class);
                startActivity(fire);
            }
        });

        ImageView thunder = (ImageView)findViewById(R.id.img_thunder);
        thunder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent thunder = new Intent(getBaseContext(),thunder_Fragment.class);
                startActivity(thunder);
            }
        });
        ImageView draught = (ImageView)findViewById(R.id.img_draught);
        draught.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent draught = new Intent(getBaseContext(),draught_Fragment.class);
                startActivity(draught);
            }
        });

        ImageView tsunami = (ImageView)findViewById(R.id.img_tsunami);
        tsunami.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tsunami = new Intent(getBaseContext(),tsunami_Fragment.class);
                startActivity(tsunami);
            }
        });


        ImageView earthquake = (ImageView)findViewById(R.id.img_earthquake);
        earthquake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent earthquake = new Intent(getBaseContext(),earthquake_Fragment.class);
                startActivity(earthquake);
            }
        });

        ImageView landslide = (ImageView)findViewById(R.id.img_landslide);
        landslide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent landslide= new Intent(getBaseContext(),landslide_Fragment.class);
                startActivity(landslide);
            }
        });


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
