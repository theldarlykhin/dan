package com.hnttechs.www.dan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

/**
 * Created by dell on 6/8/15.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    static Context c;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        c = context;
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mobile != null && mobile.isConnectedOrConnecting()) {
            Intent startIntent = new Intent(context, NotificationAlert.class);
//            startIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
            context.startService(startIntent);
        }
    }


}
