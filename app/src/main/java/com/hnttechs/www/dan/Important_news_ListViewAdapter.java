package com.hnttechs.www.dan;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/4/15.
 */
public class Important_news_ListViewAdapter extends BaseAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;

    ArrayList<ImportantNews> importantnews_listData;
    static LinearLayout news_layout;
    DBHandler handler;

    public Important_news_ListViewAdapter(Context context, ArrayList<ImportantNews> listData) {
        this.context = context;
        importantnews_listData = listData;

    }

    @Override
    public int getCount() {
        return importantnews_listData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_item, parent, false);
        }

        Typeface tf_zawgyi = Typeface.createFromAsset(context.getAssets(), "fonts/zawgyione.ttf");

        final TextView txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        final TextView txt_content = (TextView) convertView.findViewById(R.id.txt_content);
        TextView txt_continue = (TextView) convertView.findViewById(R.id.txt_continue);
        news_layout = (LinearLayout) convertView.findViewById(R.id.news_layout);
        TextView txt_date = (TextView) convertView.findViewById(R.id.txt_date);


        handler = new DBHandler(context);


        ImportantNews importantnews = importantnews_listData.get(position);
        String post_id = importantnews.getPost_id();
        String importantnewsTitle = importantnews.getTitle();
        String importantnewsContent = importantnews.getContent();
        String importantnewsDate = importantnews.getPost_date();

        txt_title.setTypeface(tf_zawgyi);
        txt_content.setTypeface(tf_zawgyi);
        txt_continue.setTypeface(tf_zawgyi);
        txt_continue.setText("ဆက္ရန္...");


        if(handler.IsRead(post_id)==true) {

            news_layout.setBackgroundColor(Color.parseColor("#c0c0c0"));
        } else {

            news_layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }


        txt_title.setText(importantnewsTitle);
        if (importantnewsContent.length() < 100) {
            txt_content.setText(importantnewsContent);
        } else {
            txt_content.setText(importantnewsContent.substring(0, 100));
        }
        txt_date.setText(importantnewsDate);

        return convertView;
    }
}