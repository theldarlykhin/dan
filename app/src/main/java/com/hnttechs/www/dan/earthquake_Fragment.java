package com.hnttechs.www.dan;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;


/**
 * Created by dell on 5/7/16.
 */
public class earthquake_Fragment extends AppCompatActivity {

    static int[] earthquake_array;

    static CustomPagerAdapter mCustomPagerAdpater;
    static ViewPager mViewPager;
    private Toolbar toolbar;
    static String category;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_storm);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.earthquake_actionbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.textColorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        earthquake_array = Dos_Donts.earthquake;

        mViewPager = (ViewPager) findViewById(R.id.pager);

        mCustomPagerAdpater = new CustomPagerAdapter(this);

        mViewPager.setAdapter(mCustomPagerAdpater);

    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        Drawable drawable;
        Bitmap bitmap;
        String ImagePath;
        Uri URI;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return earthquake_array.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imageView.setImageResource(earthquake_array[position]);
            final Vibrator vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Dialog settingsDialog = new Dialog(earthquake_Fragment.this);
                    settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog
                            , null));

                    ScaleImageView img = (ScaleImageView) settingsDialog.findViewById(R.id.scale_image);
                    img.setImageResource(earthquake_array[position]);
                    settingsDialog.show();


//                    Intent i = new Intent(storm_Fragment.this, image_dialog.class);
//                    i.putExtra("position",my_position);
//                    startActivity(i);

                }
            });



            imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    vibrator.vibrate(100);


                    AlertDialog.Builder dialogAlert = new AlertDialog.Builder(earthquake_Fragment.this);
                    dialogAlert.setTitle("Save Image?");

                    dialogAlert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            drawable = getResources().getDrawable(earthquake_array[position]);

                            bitmap = ((BitmapDrawable) drawable).getBitmap();

                            ImagePath = MediaStore.Images.Media.insertImage(
                                    getContentResolver(),
                                    bitmap,
                                    "demo_image",
                                    "demo_image"
                            );

                            URI = Uri.parse(ImagePath);

                            Toast.makeText(earthquake_Fragment.this, "Image Saved Successfully", Toast.LENGTH_LONG).show();

                        }
                    });
                    dialogAlert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialogAlert.show();

                    return false;
                }
            });


            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }
}
