package com.hnttechs.www.dan;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Splash_screen extends Activity {

    static ImageView iv,step1,step2;
    static LinearLayout step3;
    static TextView step4;
    public SharedPreferences appPreferences;
    boolean isAppInstalled = false;


    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_screen);

        step1 = (ImageView) findViewById(R.id.step1);
        step2 = (ImageView) findViewById(R.id.step2);
        step3 = (LinearLayout) findViewById(R.id.step3);
        step4 = (TextView) findViewById(R.id.text_third);

        Typeface tf_zawgyi = Typeface.createFromAsset(getAssets(), "fonts/zawgyione.ttf");

        TextView text_first = (TextView)findViewById(R.id.text_first);
        TextView text_second = (TextView)findViewById(R.id.text_second);
        text_first.setTypeface(tf_zawgyi);
        text_second.setTypeface(tf_zawgyi);

        CountDown _tik;
        _tik=new CountDown(4000,4000,this,DrawerLayout.class);// It delay the screen for 1 second and after that switch to YourNextActivity

        _tik.start();
        StartAnimations();
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.first_animation);
        anim.reset();
        step1.clearAnimation();
        step1.startAnimation(anim);


        anim = AnimationUtils.loadAnimation(this, R.anim.second_animation);
        anim.reset();
        step2.clearAnimation();
        step2.startAnimation(anim);


        anim = AnimationUtils.loadAnimation(this, R.anim.third_animation);
        anim.reset();
        step3.clearAnimation();
        step3.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.third_animation);
        anim.reset();
        step4.clearAnimation();
        step4.startAnimation(anim);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }
}
