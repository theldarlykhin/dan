package com.hnttechs.www.dan;

import java.util.ArrayList;

/**
 * Created by dell on 8/20/16.
 */
public interface NewsListener {

    public void addNews(News news);

    public ArrayList<News> getAllNews();

    public int getNewsCount();

}
