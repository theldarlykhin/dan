package com.hnttechs.www.dan;

import java.util.ArrayList;

/**
 * Created by dell on 8/20/16.
 */
public interface ImportantNewsListener {

    public void addImportantNews(ImportantNews importantnews);

    public ArrayList<ImportantNews> getAllImportant();

    public int getImportantNewsCount();

}
