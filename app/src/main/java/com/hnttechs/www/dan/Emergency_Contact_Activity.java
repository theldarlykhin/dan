package com.hnttechs.www.dan;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by dell on 7/24/16.
 */
public class Emergency_Contact_Activity extends AppCompatActivity {

    private Toolbar toolbar;
    static TextView txt_fav_name;
    static TextView txt_fav_phone;
    static RelativeLayout fav_phone_layout;
    static RelativeLayout add_contact_layout;
    File rrdDirectory;
    File file;
    private static final String json_fav_contact = "/sdcard/rrd/fav_contact.json";
    FileWriter jsonFileWriter;

    static final int PICK_CONTACT=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emergency_contact);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.emergency_actionbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.textColorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        rrdDirectory = new File("/sdcard/rrd/");
        rrdDirectory.mkdirs();
        file = new File(json_fav_contact);

        ImageView police = (ImageView) findViewById(R.id.police_phone);
        ImageView fire = (ImageView) findViewById(R.id.fire_phone);
        ImageView embulance = (ImageView) findViewById(R.id.embulance_phone);
        ImageView enquiry = (ImageView) findViewById(R.id.enquiry_phone);
        ImageView help1 = (ImageView) findViewById(R.id.help_phone1);
        ImageView help2 = (ImageView) findViewById(R.id.help_phone2);
        ImageView weather = (ImageView) findViewById(R.id.weather_phone);
        ImageView rrd = (ImageView) findViewById(R.id.rrd_phone);
        ImageView publication_phone1 = (ImageView) findViewById(R.id.publication_phone1);

        TextView txtname1 = (TextView) findViewById(R.id.txt_name1);
        TextView txtname2 = (TextView) findViewById(R.id.txt_name2);
        TextView txtname3 = (TextView) findViewById(R.id.txt_name);
        TextView txtname4 = (TextView) findViewById(R.id.txt_name4);
        TextView txtname5 = (TextView) findViewById(R.id.txt_name5);
        TextView txtname6 = (TextView) findViewById(R.id.txt_name6);
        TextView txtname7 = (TextView) findViewById(R.id.txt_name7);
        TextView txtname8 = (TextView) findViewById(R.id.txt_name8);
        TextView txtname9 = (TextView) findViewById(R.id.txt_name9);
        Button btn_add = (Button) findViewById(R.id.add_phone);
        add_contact_layout = (RelativeLayout) findViewById(R.id.add_contact_layout);
        fav_phone_layout = (RelativeLayout) findViewById(R.id.fav_contact_layout);


        RelativeLayout police_layout = (RelativeLayout) findViewById(R.id.police_layout);
        RelativeLayout fire_layout = (RelativeLayout) findViewById(R.id.fire_layout);
        RelativeLayout embulance_layout = (RelativeLayout) findViewById(R.id.embulance_layout);
        RelativeLayout enquiry_layout = (RelativeLayout) findViewById(R.id.enquiry_layout);
        RelativeLayout help_layout1 = (RelativeLayout) findViewById(R.id.help_layout1);
        RelativeLayout help_layout2 = (RelativeLayout) findViewById(R.id.help_layout2);
        RelativeLayout weather_layout = (RelativeLayout) findViewById(R.id.weather_layout);
        RelativeLayout rrd_layout = (RelativeLayout) findViewById(R.id.rrd_layout);
        RelativeLayout publication_layout1 = (RelativeLayout) findViewById(R.id.publication_layout1);



        txt_fav_phone = (TextView) findViewById(R.id.txt_fav_phoneno);
        txt_fav_name = (TextView) findViewById(R.id.txt_fav_name);

        ImageView fav_phone = (ImageView) findViewById(R.id.fav_phone);

        checkfile();

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });

        Typeface tf_zawgyi = Typeface.createFromAsset(getAssets(), "fonts/zawgyione.ttf");

        txtname1.setTypeface(tf_zawgyi);
        txtname2.setTypeface(tf_zawgyi);
        txtname3.setTypeface(tf_zawgyi);
        txtname4.setTypeface(tf_zawgyi);
        txtname5.setTypeface(tf_zawgyi);
        txtname6.setTypeface(tf_zawgyi);
        txtname7.setTypeface(tf_zawgyi);
        txtname8.setTypeface(tf_zawgyi);
        txtname9.setTypeface(tf_zawgyi);


        police_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:199"));
                startActivity(callIntent);
            }
        });
        fire_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:191"));
                startActivity(callIntent);
            }
        });
        embulance_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:192"));
                startActivity(callIntent);
            }
        });
        enquiry_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:100"));
                startActivity(callIntent);
            }
        });
        help_layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:067404666"));
                startActivity(callIntent);
            }
        });
        help_layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:067404777"));
                startActivity(callIntent);
            }
        });
        weather_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:1876"));
                startActivity(callIntent);
            }
        });
        rrd_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:098636603"));
                startActivity(callIntent);
            }
        });
        publication_layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:067404222"));
                startActivity(callIntent);
            }
        });

        fav_phone_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    InputStream jsonStream = new FileInputStream(json_fav_contact);
                    JSONObject jsonObject = new JSONObject(AdditionalMethod.InputStreamToString(jsonStream));

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + jsonObject.getString("phone")));
                    startActivity(callIntent);
                }catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
        });

    }


    private void checkfile() {

            if (!file.exists()) {

             add_contact_layout.setVisibility(View.VISIBLE);
                fav_phone_layout.setVisibility(View.GONE);
            }
            else if (file.exists()) {
                add_contact_layout.setVisibility(View.GONE);
                fav_phone_layout.setVisibility(View.VISIBLE);
                try {
                    InputStream jsonStream = new FileInputStream(json_fav_contact);
                    JSONObject jsonObject = new JSONObject(AdditionalMethod.InputStreamToString(jsonStream));



                    txt_fav_phone.setText(jsonObject.getString("phone"));
                    txt_fav_name.setText(jsonObject.getString("name"));

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }

            }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, DrawerLayout.class);
                startActivity(homeIntent);
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri contact = data.getData();
        ContentResolver cr = getContentResolver();

        Cursor c = managedQuery(contact, null, null, null, null);
        //      c.moveToFirst();


        while (c.moveToNext()) {
            String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));

            String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                try {

                    while (pCur.moveToNext()) {
                        String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        Toast.makeText(getBaseContext(),"Successfully added favourite contact",Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("name", name);
                        jsonObject.put("phone", phone);


                        jsonFileWriter = new FileWriter(json_fav_contact);
                        jsonFileWriter.write(jsonObject.toString());
                        jsonFileWriter.flush();
                        jsonFileWriter.close();

                        fav_phone_layout.setVisibility(View.VISIBLE);
                        add_contact_layout.setVisibility(View.GONE);
                        txt_fav_phone.setText(phone);
                        txt_fav_name.setText(name);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }

        }
    }
}
