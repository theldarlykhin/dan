package com.hnttechs.www.dan;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by dell on 8/25/16.
 */
public class CustomPopUpNotification extends Activity {

    static String title;
    static String content;
    static String post_date;
    static String post_id;
    static String url;
    static String post_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_popup_notification);

        Intent i = getIntent();
        title = i.getStringExtra("title");
        content = i.getStringExtra("content");
        post_date = i.getStringExtra("post_date");
        post_id = i.getStringExtra("post_id");
        url = i.getStringExtra("url");
        post_url= i.getStringExtra("post_url");


        TextView txt_title = (TextView) findViewById(R.id.txt_title);
        TextView txt_content = (TextView) findViewById(R.id.txt_content);
        TextView txt_date = (TextView) findViewById(R.id.txt_date);
        Button btn_close = (Button) findViewById(R.id.btn_close);
        Button btn_view = (Button) findViewById(R.id.btn_view);

        Typeface tf_zawgyi = Typeface.createFromAsset(getAssets(), "fonts/zawgyione.ttf");

        txt_title.setText(title.substring(0,100));
        txt_title.setTypeface(tf_zawgyi);
        txt_content.setText(content.substring(0,100));
        txt_content.setTypeface(tf_zawgyi);
        txt_date.setText(post_date);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), popup_Detail.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                i.putExtra("post_id", post_id);
                i.putExtra("title", title);
                i.putExtra("content", content);
                i.putExtra("url", url);
                i.putExtra("date", post_date);
                i.putExtra("post_url", post_url);

                startActivity(i);
                finish();
            }
        });

    }
}
