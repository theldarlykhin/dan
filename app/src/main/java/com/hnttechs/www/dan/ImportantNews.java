package com.hnttechs.www.dan;

/**
 * Created by dell on 8/20/16.
 */
public class ImportantNews {
    private int id;
    private String post_id;
    private String title;
    private String content;
    private String post_date;
    private String url;
    private String post_url;

    public ImportantNews() {
    }

    public ImportantNews(String post_id, String title, String content, String post_date, String url, String post_url) {
        this.post_id = post_id;
        this.title = title;
        this.content = content;
        this.post_date = post_date;
        this.url = url;
        this.post_url = post_url;
    }

    public ImportantNews(int id, String post_id, String title, String content, String post_date, String url, String post_url) {
        this.id = id;
        this.post_id = post_id;
        this.title = title;
        this.content = content;
        this.post_date = post_date;
        this.url = url;
        this.post_url = post_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id= post_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content= content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url= url;
    }


    public String getPost_url() {
        return post_url;
    }

    public void setPost_url(String post_url) {
        this.post_url= post_url;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

}
