package com.hnttechs.www.dan;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by dell on 9/28/16.
 */
public class Activity_AboutUs extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.about_us);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.textColorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        Typeface tf_zawgyi = Typeface.createFromAsset(getAssets(), "fonts/zawgyione.ttf");


        TextView txt_about_us = (TextView)findViewById(R.id.txt_about_us);
        txt_about_us.setTypeface(tf_zawgyi);
        txt_about_us.setText("Disaster Alert Notification (DAN) is developed by Relief and Resettlement Department, Ministry of Social Welfare, Relief and Resettlement to provide actionable disaster- related information to communities in Myanmar to be better prepared for and respond to disaster events.\n" +
                "\n" +
                "ျပည္သူမ်ား သဘာ၀ေဘးအႏၲရာယ္မ်ား ၾကံဳေတြ႕ရပါက ၾကိဳတင္ျပင္ဆင္ႏိုုင္ေစရန္ႏွင့္ အေရးေပၚတံုု႕ျပန္မႈမ်ား အခ်ိန္မီေဆာင္ရြက္ႏိုုင္ေစရန္ ရည္ရြယ္၍ လူမႈ၀န္ထမ္း၊ ကယ္ဆယ္ ေရးႏွင့္ ျပန္လည္ေနရာခ်ထားေရး၀န္ၾကီးဌာန၊ ကယ္ဆယ္ေရးႏွင့္ ျပန္လည္ေနရာခ်ထားေရး ဦးစီးဌာနမွ သဘာ၀ေဘးအႏၲရာယ္္ ၾကိဳတင္သတိေပးျခင္းဆုုိင္ရာ (Moblie Application) ကိုု ေရးဆြဲထားျခင္းျဖစ္ပါသည္။ \n");

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
