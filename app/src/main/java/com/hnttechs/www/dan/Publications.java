package com.hnttechs.www.dan;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by dell on 7/25/16.
 */
public class Publications extends AppCompatActivity {

    private Toolbar toolbar;
    static Button btn_one;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.publications);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.publication_actionbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.textColorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        TextView btn_labutta = (TextView) findViewById(R.id.btn_labutta);
        TextView btn_community = (TextView) findViewById(R.id.btn_community);
        TextView btn_management = (TextView) findViewById(R.id.btn_management);
        TextView btn_law = (TextView) findViewById(R.id.btn_law);
        TextView btn_sandai_eng= (TextView) findViewById(R.id.btn_sandai_eng);
        TextView btn_asean_guide= (TextView) findViewById(R.id.btn_asean_guide);
        TextView btn_mm_version_dm_rules= (TextView) findViewById(R.id.btn_mm_version_dm_rules);

        Typeface tf_zawgyi = Typeface.createFromAsset(getAssets(), "fonts/zawgyione.ttf");
        btn_labutta.setTypeface(tf_zawgyi);
        btn_community.setTypeface(tf_zawgyi);
        btn_management.setTypeface(tf_zawgyi);
        btn_law.setTypeface(tf_zawgyi);
        btn_sandai_eng.setTypeface(tf_zawgyi);
        btn_asean_guide.setTypeface(tf_zawgyi);
        btn_mm_version_dm_rules.setTypeface(tf_zawgyi);


        btn_labutta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdf_read("compressed_labutta_tdmpresize.pdf");
            }
        });

        btn_community.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdf_read("myanmar_national_framework_for_community_disaster_resilience.pdf");      //5.Myanmar National Framework for Community Disaster Resilience
            }
        });

        btn_management.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.themimu.info/sites/themimu.info/files/documents/Guideline_TownshipDisasterManagementPlan_RRD-GAD_Sep10_MMR.pdf"));      //7. ျမိဳ႕နယ္ သဘာ၀ေဘး စီမံခန္႕ခြဲမႈ လုုပ္ငန္းစီမံခ်က္ေရးဆြဲေရး လမ္းညႊန္
                startActivity(intent);

            }
        });

        btn_law.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdf_read("dm_law_user_version.pdf");        //1.သဘာ၀ေဘးအႏၱရာယ္ဆိုုင္ရာစီမံခန္႕ခြဲမႈ ဥပေဒ
            }
        });

        btn_sandai_eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdf_read("sendai_framework_for_disaster_risk_reduction.pdf");       //3. ေဘးအႏၱရာယ္ေၾကာင့္ ထိခိုုက္ဆံုုးရႈံးမႈ ေလၽွာ႕ခ်ေရးဆိုုင္ရာ ဆန္ဒိုုင္းမူေဘာင္
            }
        });
        btn_asean_guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdf_read("asean_reference_guide.pdf");      //4.ASEAN Disaster Recovery Reference Guide
            }
        });

        btn_mm_version_dm_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdf_read("mm_version_of_dm_rules.pdf");      //2.သဘာ၀ေဘးအႏၱရာယ္ဆိုုင္ရာစီမံခန္႕ခြဲမႈ နည္းဥေပေဒ
            }
        });

    }

    public void pdf_read(String pdf_name) {
        AssetManager assetManager = getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(getFilesDir(), pdf_name);
        try
        {
            in = assetManager.open(pdf_name);
            out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e)
        {
            Log.e("tag", e.getMessage());
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(
                Uri.parse("file://" + getFilesDir() + "/"+pdf_name),
                "application/pdf");

        startActivity(intent);




    }


    private void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}

