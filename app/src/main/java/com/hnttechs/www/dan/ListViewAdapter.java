package com.hnttechs.www.dan;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 7/4/15.
 */
public class ListViewAdapter extends BaseAdapter {
    // Declare Variables
    Context context;
    LayoutInflater inflater;

    ArrayList<News> news_listData;
    static LinearLayout news_layout;
    DBHandler handler;

    public ListViewAdapter(Context context, ArrayList<News> listData) {
        this.context = context;
        news_listData = listData;

    }

    @Override
    public int getCount() {
        return news_listData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_item, parent, false);
        }

        Typeface tf_zawgyi = Typeface.createFromAsset(context.getAssets(), "fonts/zawgyione.ttf");

        final TextView txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        final TextView txt_content = (TextView) convertView.findViewById(R.id.txt_content);
        TextView txt_continue = (TextView) convertView.findViewById(R.id.txt_continue);
        news_layout = (LinearLayout) convertView.findViewById(R.id.news_layout);
        TextView txt_date = (TextView) convertView.findViewById(R.id.txt_date);


        handler = new DBHandler(context);

        News news = news_listData.get(position);
        String post_id = news.getPost_id();
        String newsTitle = news.getTitle();
        String newsContent = news.getContent();
        String newsDate = news.getPost_date();

        if(handler.IsRead(post_id)==true) {

            news_layout.setBackgroundColor(Color.parseColor("#c0c0c0"));
        } else {

            news_layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        txt_title.setTypeface(tf_zawgyi);
        txt_content.setTypeface(tf_zawgyi);
        txt_continue.setTypeface(tf_zawgyi);
        txt_continue.setText("ဆက္ရန္...");


        txt_title.setText(newsTitle);
        if (newsContent.length() < 100) {
            txt_content.setText(newsContent);
        } else {
            txt_content.setText(newsContent.substring(0, 100));
        }
        txt_date.setText(newsDate);

        return convertView;
    }
}