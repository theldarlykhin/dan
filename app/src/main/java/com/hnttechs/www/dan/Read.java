package com.hnttechs.www.dan;

/**
 * Created by dell on 8/24/16.
 */
public class Read {

    private int id;
    private String post_id;

    public Read() {
    }

    public Read(String post_id) {
        this.post_id = post_id;
    }

    public Read(int id, String post_id) {
        this.id = id;
        this.post_id = post_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id= post_id;
    }
}
