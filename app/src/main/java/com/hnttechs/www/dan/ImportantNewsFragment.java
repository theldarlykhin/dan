package com.hnttechs.www.dan;


import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.SyncStateContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;


/**
 * Created by dell on 5/7/16.
 */
public class ImportantNewsFragment extends Fragment {

    FullLengthListView listview;
    static Important_news_ListViewAdapter adapter;
    static String serverData;
    static Integer pagination;
    private SwipeRefreshLayout swipeContainer;
    ArrayList<ImportantNews> importantnewsArrayList;
    boolean loadingMore = false;
    static DBHandler handler;
    static NetworkUtils utils;
    static Integer post_count;
    static Integer unread_count;
    boolean flag = true;
    private int preLast;
    static String first_install;
    static String is_swipe_up;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_two, container, false);
        listview = (FullLengthListView) rootView.findViewById(R.id.listview);
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);

        utils = new NetworkUtils(getActivity());
        handler = new DBHandler(getActivity());

        pagination = 1;
        post_count = 0;
        unread_count = 0;
        first_install="false";
        is_swipe_up = "no";

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isInternetOn() == true) {
                    pagination = 1;
                    swipeContainer.setRefreshing(true);
                    new Delete_and_DataFetcherTask().execute();
                } else {
                    Toast.makeText(getActivity(), "No Internet Access", Toast.LENGTH_SHORT).show();
                    swipeContainer.setRefreshing(false);
                }
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                if (handler.getImportantNewsCount() == 0) {
                    change_notification_count();
                    first_install="true";
                    if (isInternetOn() == true) {
                        new CountDownTimer(1000, 1000) {

                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                pagination = 1;
                                new DataFetcherTask().execute();
                            }
                        }.start();
                    } else {
                        Toast.makeText(getActivity(), "No Internet Access", Toast.LENGTH_SHORT).show();
                        swipeContainer.setRefreshing(false);
                    }
                } else {

                    if (isInternetOn() == true) {
                        pagination = 1;
                        swipeContainer.setRefreshing(true);
                        new Delete_and_DataFetcherTask().execute();
                    } else {

                        final ArrayList<ImportantNews> importantnewsList = handler.getAllImportant();
                        adapter = new Important_news_ListViewAdapter(getActivity(), importantnewsList);
                        listview.setAdapter(adapter);
                        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                Intent detail = new Intent(getActivity().getBaseContext(), Detail.class);
                                detail.putExtra("post_id", importantnewsList.get(position).getPost_id());
                                detail.putExtra("title", importantnewsList.get(position).getTitle());
                                detail.putExtra("content", importantnewsList.get(position).getContent());
                                detail.putExtra("url", importantnewsList.get(position).getUrl());
                                detail.putExtra("date", importantnewsList.get(position).getPost_date());
                                detail.putExtra("post_url", importantnewsList.get(position).getPost_url());
                                startActivity(detail);
                            }
                        });

                        for (int i = 0; i < importantnewsList.size(); i++) {
                            if (handler.IsRead(importantnewsList.get(i).getPost_id()) != true) {
                                unread_count++;

                            }
                        }
                        change_notification_count();
                        change_icon();


                        Toast.makeText(getActivity(), "No Internet Access", Toast.LENGTH_SHORT).show();
                        swipeContainer.setRefreshing(false);
                    }
                }
            }
        });

        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                is_swipe_up = "yes";
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if (post_count > 0) {
                    if ((lastInScreen == totalItemCount) && !(loadingMore)) {
                        if(preLast!=lastInScreen) {
                            pagination++;
                            new DataFetcherTask().execute();
                            post_count = 0;
                            preLast = lastInScreen;
                        }
                    }
                }
            }
        });
        return rootView;
    }

    public static void check_notification_count() {
        final ArrayList<ImportantNews> importantnewsList = handler.getAllImportant();
        for (int i = 0; i < importantnewsList.size(); i++) {
            if (handler.IsRead(importantnewsList.get(i).getPost_id()) != true) {
                unread_count++;
            }
        }
        change_notification_count();
    }


    public void change_icon() {

        Integer icon_count = 0;
        final ArrayList<ImportantNews> importantnewsList = handler.getAllImportant();
        for (int i = 0; i < importantnewsList.size(); i++) {
            if (handler.IsRead(importantnewsList.get(i).getPost_id()) != true) {
                icon_count++;
            }
        }

        if (icon_count == 0) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
        if (icon_count == 1) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 2) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 3) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 4) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 5) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 6) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 7) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 8) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 9) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count > 9) {
            getActivity().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getActivity().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static void change_notification_count() {

        if (unread_count == 0) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news0);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count == 1) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news1);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count == 2) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news2);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count == 3) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news3);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count == 4) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news4);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count == 5) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news5);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count == 6) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news6);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count == 7) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news7);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count == 8) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news8);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count == 9) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news9);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        } else if (unread_count > 9) {
            MainActivity.tabLayout.getTabAt(1).setIcon(R.drawable.breaking_news9plus);
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(100);
        }

        unread_count = 0;
    }


    public final boolean isInternetOn() {
        ConnectivityManager connec =
                (ConnectivityManager) getActivity().getBaseContext().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);
        }

        @Override

        protected Void doInBackground(Void... params) {
            serverData = null;
            String newsUrl;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.rrdmyanmar.gov.mm/?json=get_category_posts&id=10get_posts&page=" + pagination);
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Http Request Code end
            // Json Parsing Code Start
            try {
                importantnewsArrayList = new ArrayList<ImportantNews>();
                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("posts");
                post_count = jsonObject.getInt("count");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectImportantNews = jsonArray.getJSONObject(i);
                    String newsPostId = jsonObjectImportantNews.getString("id");
                    String newsTitle = Rabbit.uni2zg(jsonObjectImportantNews.getString("title"));
                    String newsContent = Rabbit.uni2zg(jsonObjectImportantNews.getString("content"));
                    String newsPostDate = jsonObjectImportantNews.getString("date");

                    if (jsonObjectImportantNews.has("thumbnail_images")) {
                        newsUrl = jsonObjectImportantNews.getJSONObject("thumbnail_images").
                                getJSONObject("full").getString("url");
                    } else {
                        newsUrl = "null";
                    }

                    String newsPostUrl = jsonObjectImportantNews.getString("url");

                    ImportantNews importantnews = new ImportantNews();
                    importantnews.setPost_id(newsPostId);
                    importantnews.setTitle(newsTitle);
                    importantnews.setContent(newsContent);
                    importantnews.setPost_date(newsPostDate);
                    importantnews.setUrl(newsUrl);
                    importantnews.setPost_url(newsPostUrl);
                    handler.addImportantNews(importantnews);// Inserting into DB
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
                flag = false;
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            //Json Parsing code end

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            swipeContainer.setRefreshing(false);

            final ArrayList<ImportantNews> importantnewsList = handler.getAllImportant();
            adapter = new Important_news_ListViewAdapter(getActivity(), importantnewsList);
            listview.setAdapter(adapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent detail = new Intent(getActivity().getBaseContext(), Detail.class);
                    detail.putExtra("post_id", importantnewsList.get(position).getPost_id());
                    detail.putExtra("title", importantnewsList.get(position).getTitle());
                    detail.putExtra("content", importantnewsList.get(position).getContent());
                    detail.putExtra("url", importantnewsList.get(position).getUrl());
                    detail.putExtra("date", importantnewsList.get(position).getPost_date());
                    detail.putExtra("post_url", importantnewsList.get(position).getPost_url());
                    startActivity(detail);
                }
            });

            listview.setSelection(importantnewsList.size()-11);

//            if(!flag) {
//                Toast.makeText(getActivity(), "No Internet Access",
//                        Toast.LENGTH_SHORT).show();
//            }
            for (int i = 0; i < importantnewsList.size(); i++) {
                if (handler.IsRead(importantnewsList.get(i).getPost_id()) != true) {
                    unread_count++;
                }
            }

            change_notification_count();
            if(first_install.equals("true")){
                first_install="false";
                change_icon();
            } else {
                change_icon();
            }

            if(is_swipe_up.equals("yes")) {
                MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(255);
            }
        }
    }


    class Delete_and_DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);
        }

        @Override

        protected Void doInBackground(Void... params) {
            serverData = null;
            String newsUrl;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.rrdmyanmar.gov.mm/?json=get_category_posts&id=10");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Http Request Code end
            // Json Parsing Code Start
            try {
                importantnewsArrayList = new ArrayList<ImportantNews>();
                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("posts");
                post_count = jsonObject.getInt("count");
                preLast = 0;

                if (jsonArray.length() != 0) {
                    handler.deleteImportantNewsData();
                }


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectImportantNews = jsonArray.getJSONObject(i);
                    String newsPostId = jsonObjectImportantNews.getString("id");
                    String newsTitle = Rabbit.uni2zg(jsonObjectImportantNews.getString("title"));
                    String newsContent = Rabbit.uni2zg(jsonObjectImportantNews.getString("content"));
                    String newsPostDate = jsonObjectImportantNews.getString("date");


                    if (jsonObjectImportantNews.has("thumbnail_images")) {
                        newsUrl = jsonObjectImportantNews.getJSONObject("thumbnail_images").
                                getJSONObject("full").getString("url");
                    } else {
                        newsUrl = "null";
                    }


                    String newsPostUrl = jsonObjectImportantNews.getString("url");

                    ImportantNews importantnews = new ImportantNews();
                    importantnews.setPost_id(newsPostId);
                    importantnews.setTitle(newsTitle);
                    importantnews.setContent(newsContent);
                    importantnews.setPost_date(newsPostDate);
                    importantnews.setUrl(newsUrl);
                    importantnews.setPost_url(newsPostUrl);
                    handler.addImportantNews(importantnews);// Inserting into DB
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
                flag = false;
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            //Json Parsing code end

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            swipeContainer.setRefreshing(false);

            final ArrayList<ImportantNews> importantnewsList = handler.getAllImportant();
            adapter = new Important_news_ListViewAdapter(getActivity(), importantnewsList);
            listview.setAdapter(adapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent detail = new Intent(getActivity().getBaseContext(), Detail.class);
                    detail.putExtra("post_id", importantnewsList.get(position).getPost_id());
                    detail.putExtra("title", importantnewsList.get(position).getTitle());
                    detail.putExtra("content", importantnewsList.get(position).getContent());
                    detail.putExtra("url", importantnewsList.get(position).getUrl());
                    detail.putExtra("date", importantnewsList.get(position).getPost_date());
                    detail.putExtra("post_url", importantnewsList.get(position).getPost_url());
                    startActivity(detail);
                }
            });


//            if(!flag) {
//                Toast.makeText(getActivity(), "No Internet Access",
//                        Toast.LENGTH_SHORT).show();
//            }
            for (int i = 0; i < importantnewsList.size(); i++) {
                if (handler.IsRead(importantnewsList.get(i).getPost_id()) != true) {
                    unread_count++;
                }
            }

            change_notification_count();
            change_icon();
            MainActivity.tabLayout.getTabAt(1).getIcon().setAlpha(255);
        }
    }
}