package com.hnttechs.www.dan;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.Toast;
import android.widget.Toolbar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dell on 6/10/15.
 */

public class NotificationAlert extends Service {

    private static final String TAG = NotificationAlert.class.getSimpleName();
    static Integer count = 0;
    static String title;
    static String test;
    DBHandler handler;
    static NetworkUtils utils;
    static String serverData;
    static Integer pagination;
    private static Timer timer = new Timer();
    ArrayList<ImportantNews> importantnewsArrayList;
    String first_install;
    static Bitmap bitmap_message;
    static Bitmap bitmap;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        handler = new DBHandler(getBaseContext());
        utils = new NetworkUtils(this);
        pagination = 1;
        new DataFetcherTask().execute();
        if (isInternetOn() == true) {
            timer.scheduleAtFixedRate(new mainTask(), 0, 7200000);
        }

        return START_STICKY;
    }

    private class mainTask extends TimerTask {
        public void run() {
            if (isInternetOn() == true) {
                if (handler.getImportantNewsCount() == 0) {
                    first_install = "yes";
                } else {
                    first_install = "no";
                }
                new DataFetcherTask().execute();
            }
        }
    }

    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.rrdmyanmar.gov.mm/?json=get_category_posts&id=10");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void args) {
            try {
                String newsUrl;
                importantnewsArrayList = new ArrayList<ImportantNews>();
                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("posts");
                Log.w("myApp", Integer.toString(jsonArray.length()));
                for (int i = jsonArray.length()-1; i >= 0; i--) {
                    JSONObject jsonObjectImportantNews = jsonArray.getJSONObject(i);
                    String newsPostId = jsonObjectImportantNews.getString("id");
                    String newsTitle = Rabbit.uni2zg(jsonObjectImportantNews.getString("title"));
                    String newsContent = Rabbit.uni2zg(jsonObjectImportantNews.getString("content"));
                    String newsPostDate = jsonObjectImportantNews.getString("date");

                    if (jsonObjectImportantNews.has("thumbnail_images")) {
                        newsUrl = jsonObjectImportantNews.getJSONObject("thumbnail_images").
                                getJSONObject("full").getString("url");
                    } else {
                        newsUrl = "null";
                    }

                    String newsPostUrl = jsonObjectImportantNews.getString("url");

                    if (handler.CheckIsDataAlreadyInDBorNot(newsPostId) == false) {

                        generateNotification(getBaseContext(), newsPostDate,
                                newsPostId, newsTitle, newsContent, newsUrl, newsPostUrl);

                        if (first_install.equals("no")) {
                            generatePopUpNotification(getBaseContext(), newsTitle, newsContent, newsPostDate,
                                    newsUrl, newsPostUrl, newsPostId);
                        } else if (first_install.equals("yes")) {

                            if (i == 0) {
                                generatePopUpNotification(getBaseContext(), newsTitle, newsContent, newsPostDate,
                                        newsUrl, newsPostUrl, newsPostId);
                            }
                        } else {
                            continue;
                        }

                    ImportantNews importantnews = new ImportantNews();
                    importantnews.setPost_id(newsPostId);
                    importantnews.setTitle(newsTitle);
                    importantnews.setContent(newsContent);
                    importantnews.setPost_date(newsPostDate);
                    importantnews.setUrl(newsUrl);
                    importantnews.setPost_url(newsPostUrl);
                    handler.addImportantNews(importantnews);// Inserting into DB
                }
            }
        }

        catch(JSONException e) {
            e.printStackTrace();
        } catch(NullPointerException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
}

    private static void generatePopUpNotification(final Context context,
                                                  String noti_title, String message, String post_date,
                                                  String url, String post_url, String post_id) {


        Intent i = new Intent(context, CustomPopUpNotification.class);
        i.putExtra("title", noti_title);
        i.putExtra("content", message);
        i.putExtra("post_date", post_date);
        i.putExtra("post_id", post_id);
        i.putExtra("url", url);
        i.putExtra("post_url", post_url);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }


    private static void generateNotification(Context context,String post_date,
                                             String post_id, String post_title, String content,
                                             String url, String post_url) {
        int icon = R.drawable.mswrr;
        Date date = new Date();
        int notificationid = (int) date.getTime();
        long when = System.currentTimeMillis();

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, content, when);
        String title = context.getString(R.string.app_name);
        Intent notificationIntent = new Intent(context, popup_Detail.class);
        notificationIntent.putExtra("post_id", post_id);
        notificationIntent.putExtra("title", post_title);
        notificationIntent.putExtra("content", content);
        notificationIntent.putExtra("url", url);
        notificationIntent.putExtra("date", post_date);
        notificationIntent.putExtra("post_url", post_url);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Intent notificationMainIntent = new Intent(context, DrawerLayout.class);
        PendingIntent intent = PendingIntent.getActivities(context,
                notificationid, new Intent[]{notificationMainIntent, notificationIntent}, 0);

        RemoteViews contentView = new RemoteViews(context
                .getApplicationContext().getPackageName(),
                R.layout.custom_notification);
        contentView.setImageViewResource(R.id.img_noti_logo,
                R.drawable.mswrr);

        if (post_title.length() >= 100) {
            bitmap = textAsBitmap(context, post_title.substring(0,100)+"...", 17, Color.WHITE);
        } else {
            bitmap = textAsBitmap(context, post_title+"...", 17, Color.WHITE);
        }
        contentView.setImageViewBitmap(R.id.noti_title, bitmap);

        if (content.length() >= 100) {
            bitmap_message = textAsBitmap(context, content.substring(0,100)+"...", 16, Color.WHITE);
        } else {
            bitmap_message = textAsBitmap(context, content+"...", 16, Color.WHITE);
        }
        contentView.setImageViewBitmap(R.id.noti_content, bitmap_message);
        contentView.setTextViewText(R.id.noti_date, post_date);
        notification.contentView = contentView;
        notification.contentIntent = intent;
        notification.sound = soundUri;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(notificationid, notification);
    }


    public final boolean isInternetOn() {
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }



    public static Bitmap textAsBitmap(Context context,String messageText,float textSize,int textColor){
        String fontName="zawgyi";
        Typeface font=Typeface.createFromAsset(context.getAssets(),String.format("fonts/zawgyione.ttf",fontName));
        Paint paint=new Paint();
        paint.setTextSize(textSize);
        paint.setTypeface(font);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline=-paint.ascent(); // ascent() is negative
        int width=(int)(paint.measureText(messageText)+0.5f); // round
        int height=(int)(baseline+paint.descent()+0.5f);
        Bitmap image=Bitmap.createBitmap(width,height,Bitmap.Config.ARGB_8888);
        Canvas canvas=new Canvas(image);
        canvas.drawText(messageText,0,baseline,paint);
        return image;
    }
}
