package com.hnttechs.www.dan;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by dell on 5/8/16.
 */
public class Detail extends AppCompatActivity {

    private Toolbar toolbar;
    static String post_id;
    static String title;
    static String content;
    static String read_id;
    static String status;
    static String url_photo;
    static String date;
    static String post_url;
    ImageLoader imageLoader = new ImageLoader(this);
    DBHandler handler;
    static ImageView img_photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setIcon(R.drawable.full_logo);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.textColorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        Intent i = getIntent();
        post_id = i.getStringExtra("post_id");
        title = i.getStringExtra("title");
        content = i.getStringExtra("content");
        url_photo = i.getStringExtra("url");
        date = i.getStringExtra("date");
        post_url = i.getStringExtra("post_url");

        handler = new DBHandler(getBaseContext());
        Read read = new Read();
        read.setPost_id(post_id);
        if(handler.IsRead(post_id)== false) {
            handler.addRead(read);
        }

        img_photo = (ImageView) findViewById(R.id.img_news_photo);
        TextView txt_title = (TextView) findViewById(R.id.txt_title);
        TextView txt_content = (TextView) findViewById(R.id.txt_content);
        TextView txt_time = (TextView) findViewById(R.id.txt_time);
        Button btn_share = (Button) findViewById(R.id.btn_share);

        Typeface tf_zawgyi = Typeface.createFromAsset(getAssets(), "fonts/zawgyione.ttf");


        txt_title.setTypeface(tf_zawgyi);
        txt_content.setTypeface(tf_zawgyi);
        txt_time.setTypeface(tf_zawgyi);
        txt_time.setText(date);

        txt_title.setText(title);
        txt_content.setText(content);
        if (url_photo.equals("null")) {
            img_photo.setVisibility(View.GONE);
        } else {
            imageLoader.DisplayImage(url_photo, img_photo);


            img_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Dialog settingsDialog = new Dialog(Detail.this);
                    settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_dialog
                            , null));

                    ScaleImageView img = (ScaleImageView) settingsDialog.findViewById(R.id.scale_image);
                    imageLoader.DisplayImage(url_photo, img);
                    settingsDialog.show();
                }
            });
        }
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareIt(post_url);
            }
        });
    }


    private void shareIt(String url) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Share");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, url);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    public void change_icon() {

        Integer icon_count=0;
        final ArrayList<ImportantNews> importantnewsList = handler.getAllImportant();
        for (int i = 0; i < importantnewsList.size(); i++) {
            if (handler.IsRead(importantnewsList.get(i).getPost_id()) != true) {
                icon_count++;

            }
        }

        if (icon_count == 0) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 1) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 2) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 3) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 4) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 5) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 6) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 7) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 8) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count == 9) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (icon_count > 9) {
            getBaseContext().getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9plus"),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            try {
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-0"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-2"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-3"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-4"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-5"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-6"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-7"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-8"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-9"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getBaseContext().getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.hnttechs.www.dan", "com.hnttechs.www.dan.Splash_screen-1"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        NewsFragment.adapter.notifyDataSetChanged();
        ImportantNewsFragment.adapter.notifyDataSetChanged();
        ImportantNewsFragment.check_notification_count();
        change_icon();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                NewsFragment.adapter.notifyDataSetChanged();
                ImportantNewsFragment.adapter.notifyDataSetChanged();
                ImportantNewsFragment.check_notification_count();
                change_icon();

        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
