package com.hnttechs.www.dan;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dell on 5/7/16.
 */
public class NewsFragment extends Fragment {

    static FullLengthListView listview;
    static ListViewAdapter adapter;
    static String serverData;
    private SwipeRefreshLayout swipeContainer;
    ArrayList<News> newsArrayList;
    DBHandler handler;
    boolean loadingMore = false;
    static Integer pagination;
    static NetworkUtils utils;
    static Integer post_count;
    boolean flag = true;
    private int preLast;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_one, container, false);
        listview = (FullLengthListView) rootView.findViewById(R.id.listview);
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);

        // add footer
//        View footerView = inflater.inflate(R.layout.footer, null);
//        listview.addFooterView(footerView);
//        txt_page_no = (TextView) footerView.findViewById(R.id.txt_page_no);

        utils = new NetworkUtils(getActivity());
        handler = new DBHandler(getActivity());
        pagination = 1;
        post_count = 0;

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isInternetOn() == true) {
                    pagination = 1;
                    swipeContainer.setRefreshing(true);
                    new Delete_and_DataFetcherTask().execute();
                } else {
                    Toast.makeText(getActivity(), "No Internet Access", Toast.LENGTH_SHORT).show();
                    swipeContainer.setRefreshing(false);
                }
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);


                if (handler.getNewsCount() == 0) {      // when app is first launched and there is no stored data.
                    if (isInternetOn() == true) {
                        pagination = 1;
                        swipeContainer.setRefreshing(true);
                        new DataFetcherTask().execute();
                    } else {
                        Toast.makeText(getActivity(), "No Internet Access", Toast.LENGTH_SHORT).show();
                        swipeContainer.setRefreshing(false);
                    }
                } else {    // when there is stored data.

                    if (isInternetOn() == true) {
                        swipeContainer.setRefreshing(true);
                        pagination = 1;
                        new Delete_and_DataFetcherTask().execute();
                    } else {

                        final ArrayList<News> newsList = handler.getAllNews();
                        adapter = new ListViewAdapter(getActivity(), newsList);
                        listview.setAdapter(adapter);
                        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent detail = new Intent(getActivity().getBaseContext(), Detail.class);
                                detail.putExtra("post_id", newsList.get(position).getPost_id());
                                detail.putExtra("title", newsList.get(position).getTitle());
                                detail.putExtra("content", newsList.get(position).getContent());
                                detail.putExtra("url", newsList.get(position).getUrl());
                                detail.putExtra("date", newsList.get(position).getPost_date());
                                detail.putExtra("post_url", newsList.get(position).getPost_url());
                                startActivity(detail);
                            }
                        });

                        Toast.makeText(getActivity(), "No Internet Access", Toast.LENGTH_SHORT).show();
                        swipeContainer.setRefreshing(false);
                    }
                }

            }
        });
        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;

                if (post_count > 0) {
                    if ((lastInScreen == totalItemCount) && !(loadingMore)) {

                        if(preLast!=lastInScreen) {
                            pagination++;
                            new DataFetcherTask().execute();
                            preLast = lastInScreen;
                        }
                    }
                }
            }
        });

        if (isInternetOn() == true) {
//            if(!DrawerLayout.from.equals("noti")) {
                Intent intent = new Intent(getActivity(), NotificationAlert.class);
                getActivity().startService(intent);
//            }
        }

        return rootView;
    }


    public final boolean isInternetOn() {
        ConnectivityManager connec =
                (ConnectivityManager) getActivity().getBaseContext().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }


    class DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;
            String newsUrl;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.rrdmyanmar.gov.mm/?json=get_category_posts&id=1get_posts&page=" + pagination);
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Http Request Code end
            // Json Parsing Code Start
            try {
                newsArrayList = new ArrayList<News>();
                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("posts");
                post_count = jsonObject.getInt("count");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectNews = jsonArray.getJSONObject(i);
                    String newsPostId = jsonObjectNews.getString("id");
                    String newsTitle = Rabbit.uni2zg(jsonObjectNews.getString("title"));
                    String newsContent = Rabbit.uni2zg(jsonObjectNews.getString("content"));
                    String newsPostDate = jsonObjectNews.getString("date");

                    if (jsonObjectNews.has("thumbnail_images")) {
                        newsUrl = jsonObjectNews.getJSONObject("thumbnail_images").
                                getJSONObject("full").getString("url");
                    } else {
                        newsUrl = "null";
                    }


                    String newsPostUrl = jsonObjectNews.getString("url");

                    News news = new News();
                    news.setPost_id(newsPostId);
                    news.setTitle(newsTitle);
                    news.setContent(newsContent);
                    news.setPost_date(newsPostDate);
                    news.setUrl(newsUrl);
                    news.setPost_url(newsPostUrl);

                    handler.addNews(news);// Inserting into DB
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
                flag = false;
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            //Json Parsing code end
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            swipeContainer.setRefreshing(false);

            final ArrayList<News> newsList = handler.getAllNews();
            adapter = new ListViewAdapter(getActivity(), newsList);
            listview.setAdapter(adapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent detail = new Intent(getActivity().getBaseContext(), Detail.class);
                    detail.putExtra("post_id", newsList.get(position).getPost_id());
                    detail.putExtra("title", newsList.get(position).getTitle());
                    detail.putExtra("content", newsList.get(position).getContent());
                    detail.putExtra("url", newsList.get(position).getUrl());
                    detail.putExtra("date", newsList.get(position).getPost_date());
                    detail.putExtra("post_url", newsList.get(position).getPost_url());
                    startActivity(detail);
                }
            });
            listview.setSelection(newsList.size()-11);

//            if(!flag) {
//                Toast.makeText(getActivity(), "No Internet Access",
//                        Toast.LENGTH_SHORT).show();
//            }
        }
    }

    class Delete_and_DataFetcherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            serverData = null;
            String newsUrl;

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://www.rrdmyanmar.gov.mm/?json=get_category_posts&id=1get_posts&page=" + pagination);
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Http Request Code end
            // Json Parsing Code Start
            try {
                newsArrayList = new ArrayList<News>();
                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("posts");
                post_count = jsonObject.getInt("count");
                preLast = 0;

                if (jsonArray.length() != 0) {
                    handler.deleteNewsData();
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectNews = jsonArray.getJSONObject(i);
                    String newsPostId = jsonObjectNews.getString("id");
                    String newsTitle = Rabbit.uni2zg(jsonObjectNews.getString("title"));
                    String newsContent = Rabbit.uni2zg(jsonObjectNews.getString("content"));
                    String newsPostDate = jsonObjectNews.getString("date");

                    if (jsonObjectNews.has("thumbnail_images")) {
                        newsUrl = jsonObjectNews.getJSONObject("thumbnail_images").
                                getJSONObject("full").getString("url");
                    } else {
                        newsUrl = "null";
                    }

                    String newsPostUrl = jsonObjectNews.getString("url");

                    News news = new News();
                    news.setPost_id(newsPostId);
                    news.setTitle(newsTitle);
                    news.setContent(newsContent);
                    news.setPost_date(newsPostDate);
                    news.setUrl(newsUrl);
                    news.setPost_url(newsPostUrl);
                    handler.addNews(news);// Inserting into DB

                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
                flag = false;
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            //Json Parsing code end
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            swipeContainer.setRefreshing(false);

            final ArrayList<News> newsList = handler.getAllNews();
            adapter = new ListViewAdapter(getActivity(), newsList);
            listview.setAdapter(adapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent detail = new Intent(getActivity().getBaseContext(), Detail.class);
                    detail.putExtra("post_id", newsList.get(position).getPost_id());
                    detail.putExtra("title", newsList.get(position).getTitle());
                    detail.putExtra("content", newsList.get(position).getContent());
                    detail.putExtra("url", newsList.get(position).getUrl());
                    detail.putExtra("date", newsList.get(position).getPost_date());
                    detail.putExtra("post_url", newsList.get(position).getPost_url());
                    startActivity(detail);
                }
            });

        }

    }
}
